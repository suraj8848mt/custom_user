from django.shortcuts import render
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, get_user_model, logout
from .forms import UserCreationForm, UserLoginForm, ProfileForm, UserUpdateForm
from django.http import HttpResponseRedirect
from django.contrib import messages

from .decorators import allowed_users
from .models import Profile, MyUser

User = get_user_model()


def home(request):
    users = User.objects.values("id", "username", "email")
    title = "user list"
    context = {"users": users, "title": title}
    return render(request, 'accounts/home.html', context)

def register(request, *args, **kwargs):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/profile')
    context = {
        'form':form
    }
    return render(request, 'accounts/register.html', context)


def login_view(request, *args, **kwargs):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        return HttpResponseRedirect('/')
    return render(request, "accounts/login.html", {"form":form})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')


@login_required(login_url='/login/')
@transaction.atomic
def update_profile(request):
    # user = User.objects.get(pk=id)
    profile = Profile.objects.filter()
    if request.method == 'POST':
        # user_form = UserCreationForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if profile_form.is_valid():
            # user_form.save()
            profile_form.save()
            messages.success(request, ('Your profile was successfully updated!'))
            return HttpResponseRedirect('/')
        else:
            messages.error(request, ('Please correct the error below'))
    else:
        # user_form = UserCreationForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'accounts/profile.html', {
        # 'user_form': user_form,
        'profile_form': profile_form
    })
    
@login_required(login_url="/login/")
def all_user_view(request):
    users = User.objects.values("id", "username", "email")
    print(users)
    title = "user list"
    context = {"users": users, "title": title, "tab_title":"Users"}
    return render(request, "accounts/list.html", context)


@login_required(login_url="/login/")
@allowed_users(allowed_roles=['admin'])
def detail_user_view(request, id):
    context = {}

    user = User.objects.get(pk=id)
    context["user"] = user
    return render(request, "accounts/detail.html", context)

@login_required(login_url="/login/")
@allowed_users(allowed_roles=['admin'])
def delete_user_view(request, id):
    selecteduser = User.objects.get(id=id)
    context = {}

    if not selecteduser.is_admin:
        if request.method == "POST":
            selecteduser.delete()
            return HttpResponseRedirect("/list/")

    else:
        context["error"] = "Cannot delete Admin"
        return render(request, "accounts/error.html", context)

    context = {
         "user": selecteduser
    }
    return render(request, "accounts/user_delete.html", context)

@login_required(login_url="/login/")
@allowed_users(allowed_roles=['admin'])
def update_user_view(request, id):
    user = User.objects.get(id=id)
    title = "Update form"
    if request.user:
        if request.method == 'POST':
            form = UserUpdateForm(request.POST, instance=user)
            update = form.save(commit=False)
            update.save()
            return HttpResponseRedirect("/list/")
        else:
            form = UserUpdateForm(instance=user)
            context = {"form": form, "title": title}
        return render(request, 'accounts/form.html', context)
    else:
        context = {"error": "Not authorized to perform updates"}
        return render(request, 'accounts/error.html', context)