from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import (
    AbstractBaseUser, 
    BaseUserManager, 
    PermissionsMixin,
    Group
    )

USERNAME_REGEX = '^[a-zA-Z0-9.+-]*$'

class MyUserManager(BaseUserManager):

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError('User must have an Email address')
        user = self.model(username=username, email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password=None):
        user = self.create_user(username, email, password=password)
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        max_length=300,
        validators=[
            RegexValidator(
                regex=USERNAME_REGEX,
                message='Username must be alphanumeric or contains numbers',
                code='Invalid Username'
                )],
        unique=True
        )
    email = models.EmailField(
        max_length=255,
        unique=True,
        verbose_name='email address')
    image = models.ImageField(upload_to='customUserApp', blank=True, null=True)
    groups = models.ManyToManyField(Group, verbose_name=_('groups'), blank=True,
                                    help_text=_(
                                        'The groups this user belongs to. A user will get all permissions '
                                        'granted to each of their groups.'
                                        ),
                                    related_name="user_set",
                                    related_query_name="user",
    )
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    
    

    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

 
class Groups(models.Model):
       role = models.CharField(max_length=50)
       
       
       def __str__(self):
           return self.role

 
class Profile(models.Model):
        user = models.OneToOneField(MyUser, on_delete=models.CASCADE)
        bio = models.TextField(max_length=500, blank=True)
        location = models.CharField(max_length=30, blank=True)
        birth_date = models.DateField(null=True, blank=True)
        
        def __str__(self):
                return self.user.username
        
@receiver(post_save, sender=MyUser)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)