

from django.urls import path

from .views import (register, 
                    login_view, 
                    logout_view, 
                    update_profile, 
                    all_user_view,
                    detail_user_view,
                    delete_user_view,
                    update_user_view,
                    logout, 
                    home
                    )

urlpatterns = [
    path('register/', register, name="register"),
    path('login/', login_view, name="login"),
    path('logout/', logout_view, name="logout"),
    path('profile/', update_profile, name="profile"),
    path('list/', all_user_view, name="list"),
    path('detail/<int:id>', detail_user_view, name="users_detail"),
    path('delete/<str:id>/', delete_user_view, name="delete_user"),
    path('update/<str:id>/', update_user_view, name="update"),
    path('logout/', logout, name="logout"),
    path('', home, name="home")
]
